export class VisOptions {
  width: string = '100%'
  height: string = '300px'
  stack: boolean = true
  showMajorLabels: boolean = true
  showCurrentTime: boolean = true
  zoomMin: number = 1000000
  format: minorLabels = {
    minorLabels: {
      minute: 'h:mma',
      hour: 'ha'
    }
  }
}

export interface minorLabels {
  minorLabels: {minute: string, hour: string}
}
