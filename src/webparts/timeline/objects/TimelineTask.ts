import * as _ from '../utils/utils'

const allowedTypes = [
    'box',
    'point',
    'range',
    'background'
]

const defaultType = allowedTypes[0]

class TimelineTask {

    constructor(task = null, taskType = null, options, gpId = null) {
        this.title = task.Title
        task.Body !== null ? this.content = _.extractTextFromHtml(task.Body) : this.content = "pas de contenu"
        this.group = _.setGroupIdforUnassignedTask(gpId, task)
        this.id = task.ID
        this.start = _.getDateFromString(task.StartDate)
        this.end = _.getDateFromString(task.DueDate)
        this.type = this.getType(taskType,  _.getDateFromString(task.StartDate), _.getDateFromString(task.DueDate))
        this.modified = _.getDateFromString(task.Modified)
        this.status = task.Status
        this.percentComplete = Math.round(task.PercentComplete * 100) + "%"
        this.priority = task.Priority
        this.created = _.getDateFromString(task.Created)
        this.editable = options
        this.startLocal = _.getDateFromString(task.StartDate).toLocaleDateString()
        this.endLocal = _.getDateFromString(task.DueDate).toLocaleDateString()
        this.modifiedLocal = _.getDateFromString(task.Modified).toLocaleDateString()
        this.createdLocal = _.getDateFromString(task.Created).toLocaleDateString()
    }

    getType(type, startdate: Date = null, EndDate: Date = null) {
        var inTypes = false
        if (type === null || type === undefined || type === '')
            return this._getTypeFromDate(startdate, EndDate);
        else {
            for (let t of allowedTypes) {
                if (t === type) {
                    inTypes = true
                    break
                }
            }
        }
        return inTypes === true ? type : defaultType
    }

    private _getTypeFromDate(startDate: Date = new Date(), endDate: Date = new Date()): string {
        let isJesusChristDate = endDate.getFullYear() === 1970;
        if (isJesusChristDate)
            return this._getPoint();
        else if (startDate !== endDate && !isJesusChristDate)
            return this._getRange();
        else if (startDate && isJesusChristDate)
            return this._getBox()
        else
            return this._getBox();
    }

    private _getRange() {
        return allowedTypes[2];
    }

    private _getPoint() {
        return allowedTypes[1];
    }

    private _getBox() {
        return defaultType;
    }

    private _getBackground() {
        return allowedTypes[3];
    }

    id
    start
    end
    created
    modified
    startLocal
    createdLocal
    modifiedLocal
    endLocal
    status
    percentComplete
    priority
    content
    title
    type
    editable
    group
}

export default TimelineTask
