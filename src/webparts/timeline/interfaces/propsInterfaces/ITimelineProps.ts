import {WebPartContext} from '@microsoft/sp-webpart-base'
export interface ITimelineProps {
  context: WebPartContext;
  list: string | string[];
  title: string;
}
