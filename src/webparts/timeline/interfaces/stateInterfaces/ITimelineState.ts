export interface ITimelineState {
  groups: any,
  tasks: any,
  option: any,
  listId: string | string []
}
