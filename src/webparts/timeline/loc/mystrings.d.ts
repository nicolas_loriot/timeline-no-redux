declare interface ITimelineWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  appState: string;
  task: string;
  list: string | string[];
  title: string;
  remove: boolean;
  updateTime: boolean;
}

declare module 'TimelineWebPartStrings' {
  const strings: ITimelineWebPartStrings;
  export = strings;
}
