import TimelineTask from '../objects/TimelineTask'

/* build the list of timeline tasks and groups */
/* return an object which is dispatched in store by getTasks (action-task.js) */

export function mapTasksToTimelineTasks(tasks = [], groups, options) {
  var timelineTasks = []
  var gpId = guidGenerator()
  var toReturn = {timeline: null, gp: null}
  if (tasks !== null && tasks.length) {
    tasks.forEach(task => {
      timelineTasks.push(new TimelineTask(task, null , options, gpId))
      var gpTemp = pushGroupInGroups(task, gpId, groups)
      if (gpTemp !== null) {
        groups.push(gpTemp)
      }
    });
  }
  toReturn.timeline = timelineTasks
  toReturn.gp = groups
  return toReturn
}

export function pushGroupInGroups(task, gpId, groups) {
  var group = {}
  if (task.AssignedTo !== null && task.AssignedTo !== undefined && task.AssignedTo.length){
    fillUser(task.AssignedTo[0], group, gpId)
  } else {
    fillUser (null, group, gpId)
  }
  group = normalizedArray(groups, group)
  return group
}

export function setGroupIdforUnassignedTask(gpId, task) {
  if (task.AssignedTo !== null && task.AssignedTo !== undefined && task.AssignedTo.length){
    return task.AssignedTo[0].Id
  }
  return gpId
}

export function fillUser(assignedTo, newUser, gpId) {
  if (assignedTo !== undefined && assignedTo !== null) {
    newUser.id = assignedTo.Id
    newUser.content = assignedTo.FirstName + " " + assignedTo.LastName
  } else {
    newUser.id = gpId
    newUser.content = "tâche non assignée"
  }
}

export function getDateFromString(dateStr) {
  return new Date(dateStr)
}

export function extractTextFromHtml(html) {
  var temp = document.createElement('p')
  temp.innerHTML = html
  return temp.textContent || temp.innerHTML || ""
}

/* remove duplicate id from a list */

export function normalizedArray(arrayFrom, input) {
  if (arrayFrom === undefined || arrayFrom === null)
    return null
  if (arrayFrom.length === 0)
    return input
  else {
    for (var i = 0; i < arrayFrom.length; i++) {
      if (arrayFrom[i].id === input.id) {
        return null
      }
    }
    return input
  }
}

export function guidGenerator() {
  var S4 = function () {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  };
  return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

export function getGroup(task, groups, gp) {
  for (let i = 0; i < groups.length; i++) {
    if (groups[i].id === task.group) {
      gp = groups[i]
    }
  }
  return gp
}
