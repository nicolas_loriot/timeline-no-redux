import * as React from 'react';
import {ITimelineProps, ITimelineState} from '../interfaces/index';
import {VisOptions} from '../objects/VisOptions'
import TimelineComponent from './TimelineComponent/TimelineComponent';
import {mapTasksToTimelineTasks} from '../utils/utils';
import axios from 'axios'

var oldId: string | string[] = ""

export default class Timeline extends React.Component<ITimelineProps, ITimelineState> {
  constructor(props) {
    super(props);
    this.state = {
      groups: [],
      option: new VisOptions(),
      tasks: [],
      listId: ""
    }
  }

  componentDidUpdate(prevProps) {
    if (oldId !== prevProps.list) {
      console.log('update', this.props, this.state)
      this._getTasks(prevProps.list)
    } else if (prevProps.list === '') {
      oldId = prevProps;
      this.setState({tasks: [], groups: []})
    }
  }

  componentDidMount() {
    if (oldId !== this.props.list) {
      console.log('mount', this.props, this.state)
      this._getTasks(this.props.list);
    }
  }

  public render(): React.ReactElement<ITimelineProps> {
    const {groups, option, tasks} = this.state
    console.log('render', this.state.tasks)
    return (
      <div>
        <h1>{this.props.title && this.props.title}</h1>
        {
          tasks.length ? <TimelineComponent groups={groups} options={option} tasks={tasks}/> :
            <div>Pas de liste sélectionnée</div>
        }
      </div>
    );
  }

  private _getTasks(listId: string | string[]) {
    const {absoluteUrl} = this.props.context.pageContext.web;
    const end = absoluteUrl + "/_api/lists/getbyid('" + listId + "')/items?$expand=AssignedTo&$select=Title,Priority, Status, PercentComplete, Created, Modified, Body, ID, StartDate, DueDate,AssignedTo/FirstName, AssignedTo/Id, AssignedTo/LastName"
    let groups = [];
    let returned = {timeline: [], gp: []};
    oldId = listId
    if (listId != undefined && listId != "")
      axios.get(end).then(r => {
        returned = mapTasksToTimelineTasks(r.data.value, groups, this.state.option)
        this.setState({groups: returned.gp, tasks: returned.timeline})
      })
  }
}

