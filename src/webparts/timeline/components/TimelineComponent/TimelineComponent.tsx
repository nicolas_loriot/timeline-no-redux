import * as React from 'react';
import Timeline from 'react-visjs-timeline';

const TimelineComponent = ({groups, options, tasks}) => {
  return (
    <div>
      <Timeline groups={groups} options={options} items={tasks}/>
    </div>
  );
};

export default TimelineComponent;
